" let theme = 'quantum'
" let theme = 'onedark'
let theme = 'palenight'
" let theme = 'hybrid-mat'
" let theme = 'dues'
" let theme = 'solarized8'
" let theme = 'ayu'
" let theme = 'night-owl'

if theme == 'solarized8'
  let g:solarized_term_italics=1
  set background=dark
  colorscheme solarized8_flat
elseif theme == 'palenight'
  let g:palenight_terminal_italics=1
  set background=dark
  colorscheme palenight
elseif theme == 'night-owl'
  colorscheme night-owl
elseif theme == 'nord'
  colorscheme nord
elseif theme == 'tender'
  colorscheme tender
  let g:airline_theme = 'tender'
elseif theme == 'hybrid-mat'
  let g:enable_bold_font = 1
  set background=dark
  colorscheme hybrid_material
elseif theme == 'hybrid-rev'
  let g:enable_bold_font = 1
  set background=dark
  colorscheme hybrid_reverse
elseif theme == 'quantum'
  let g:quantum_italics=1
  set background=dark
  colorscheme quantum
elseif theme == 'seoul'
  let g:seoul256_background = 233
  color seoul256
elseif theme == 'ayu'
  let ayucolor='mirage'
  colorscheme ayu
elseif theme == 'nova'
  colorscheme nova
elseif theme == 'dracula'
  color dracula
elseif theme == 'dues'
  " set shell variable to enable italic
  " export TERM_ITALICS=true
  colors deus
elseif theme == 'onedark'
  let g:onedark_terminal_italics=1
  let g:airline_theme='onedark'
  colorscheme onedark
elseif theme == 'gruvbox'
  let g:gruvbox_contrast_dark='dark'
  colorscheme gruvbox
elseif theme == 'one'
  let g:one_allow_italics = 1
  let g:airline_theme='one'
  set background=light
  colorscheme one
elseif theme == 'spacedark'
  colorscheme space-vim-dark
endif

