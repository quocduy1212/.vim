source ~/.vim/plug.vim
"defaults write NSGlobalDomain KeyRepeat -int 1
"defaults write NSGlobalDomain InitialKeyRepeat -int 10
"font: google drive => font
"https://github.com/be5invis/Iosevka

" vim config
syntax on
if has("termguicolors")
  set termguicolors
end
set lazyredraw
set number
set nowrap
set tabstop=2
set shiftwidth=2
set expandtab
set smartindent
set autoindent
set hidden
set history=100
set hlsearch
set showmatch
set autoread
set noswapfile
set nocompatible
set incsearch
set formatoptions+=j
set scrolloff=1
set laststatus=2
set mouse=a
set clipboard^=unnamed,unnamedplus
set wildignore+=*/tmp/*,*.so,*.swp,*.zip
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.idea/*,*/.DS_Store,*/node_modules
set noeb vb t_vb=
filetype on
filetype indent on
filetype plugin indent on
set list
" run :digraph for more chars
" set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:␣
" set listchars=eol:¬,tab:»·,extends:>,precedes:<,space:␣
" set listchars=tab:→\ ,trail:·,precedes:«,extends:»,eol:¶
" set listchars=tab:→\ ,trail:␣,extends:…,eol:⏎
" set listchars=tab:‣\ ,space:·,precedes:…,extends:…,eol:¬
set listchars=tab:‣\ ,precedes:…,extends:…

" italics escape codes from terminfo
" https://github.com/morhetz/gruvbox/issues/119
let &t_ZH="\e[3m"
let &t_ZR="\e[23m"

" ==========================================
" theme
" ==========================================
source ~/.vim/theme.vim

let mapleader=" "
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
nnoremap <tab> %
vnoremap <tab> %
nnoremap S "_diwP
nnoremap <leader>w :w<CR>
nnoremap <leader>q :q<CR>
nnoremap <leader>Q :qa!<CR>
nnoremap <leader>R :source ~/.vim/vimrc<CR>
nnoremap <leader>C :ccl<CR>
nnoremap <leader>M :only<CR>
nnoremap <leader><leader> :e#<CR>
nnoremap H :tabprevious<CR>
nnoremap L :tabnext<CR>
nnoremap <leader>fp :let @*=expand("%:p")<CR>
nnoremap <leader>fn :let @*=expand("%")<CR>
nnoremap <leader>ra ca{TYPES<Esc>oconst <c-r>" = TYPES;<Esc>
nnoremap <leader>ev :tabedit ~/.vim/vimrc<CR>
nnoremap <leader>et :tabedit ~/.tmux/tmux.conf<CR>
inoremap kj <Esc>
inoremap kJ <Esc>
inoremap Kj <Esc>
inoremap KJ <Esc>
" nnoremap <CR> i<CR><Esc>O

" ==========================================
" utils
" ==========================================
" <div /> ==> <div></div>
nnoremap <leader>d/ yiwt/dt>a</<C-R>0><esc>T>i

" remove whitespaces on saving
autocmd BufWritePre * :%s/\s\+$//e
" highlight only active buffer
" augroup CursorLine
  " au!
  " au VimEnter,WinEnter,BufWinEnter * setlocal cursorline
  " au WinLeave * setlocal nocursorline
" augroup END

" pluginconfig
" ==========================================
" vimwiki
" ==========================================
let g:vimwiki_list = [ {'path': '~/.vim/wikis/vim'}, {'path': '~/.vim/wikis/tmux'}, {'path': '~/.vim/wikis/general'}, {'path': '~/.vim/wikis/js'}, {'path': '~/.vim/wikis/crypto'} ]

" pluginconfig
" ==========================================
" neoformat
" ==========================================
" augroup neoformat_format_on_save
  " autocmd BufWritePre *.js,*.jsx,*.ts,*.tsx,*.scss,*.json,*.graphql,*.vue | Neoformat
" augroup END

" pluginconfig
" ==========================================
" prettier
" ==========================================
let g:prettier#config#config_precedence = 'file-override'
let g:prettier#config#print_width = 140
let g:prettier#config#tab_width = 2
let g:prettier#config#use_tabs = 'false'
let g:prettier#config#semi = 'true'
let g:prettier#config#single_quote = 'true'
let g:prettier#config#bracket_spacing = 'true'
let g:prettier#config#trailing_comma = 'es5'
let g:prettier#config#jsx_bracket_same_line = 'false'

" pluginconfig
" ==========================================
" undotree
" ==========================================
nnoremap <leader>u :UndotreeToggle<cr>

" pluginconfig
" ==========================================
" airline
" ==========================================
let g:airline_section_y = []
let g:airline_section_z = airline#section#create(['linenr', 'maxlinenr', ':%3v'])

" pluginconfig
" ==========================================
" nerdcommenter
" ==========================================
let g:NERDSpaceDelims = 1

" pluginconfig
" ==========================================
" easymotion
" ==========================================
map <leader>m <Plug>(easymotion-prefix)

" pluginconfig
" ==========================================
" qfenter
" ==========================================
let g:qfenter_keymap = {}
let g:qfenter_keymap.open = ['o', '<CR>']
let g:qfenter_keymap.vopen = ['s']
let g:qfenter_keymap.hopen = ['i']
let g:qfenter_keymap.topen = ['t']

" pluginconfig
" ==========================================
" nerdtree
" ==========================================
nnoremap <leader>tt :NERDTreeToggle<CR>
nnoremap <leader>tr :NERDTreeFind<CR>

" pluginconfig
" ==========================================
" fzf.vim
" ==========================================
" CTRL-A CTRL-Q to select all and build quickfix list
function! s:build_quickfix_list(lines)
  call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
  copen
  cc
endfunction
let g:fzf_history_dir = '~/.vim/fzf-history'
let g:fzf_files_options = '--bind alt-a:select-all,alt-d:deselect-all'
let g:fzf_action = {
  \ 'ctrl-q': function('s:build_quickfix_list'),
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-i': 'split',
  \ 'ctrl-s': 'vsplit' }
let $FZF_DEFAULT_OPTS = '--bind ctrl-a:select-all'
command! -bang -nargs=* Rg
      \ call fzf#vim#grep(
      \   'rg --column --line-number --no-heading --color=always '.shellescape(<q-args>), 1,
      \   <bang>0 ? fzf#vim#with_preview('up:60%')
      \           : fzf#vim#with_preview('right:50%:hidden', '?'),
      \   <bang>0)
nnoremap <Leader>fa :Files<CR>
nnoremap <Leader>fg :GFiles<CR>
nnoremap <Leader>fe :Buffers<CR>
nnoremap <Leader>fh :History<CR>
nnoremap <Leader>fl :BLines<CR>

" pluginconfig
" ==========================================
" text search ack.vim + ripgrep
" ==========================================
let g:ack_use_cword_for_empty_search = 1
let g:ackhighlight = 1
let g:ackprg = 'rg --vimgrep --no-heading --fixed-strings'
let g:ack_apply_qmappings = 0
let g:ack_apply_lmappings = 0
nnoremap <Leader>sb :Ack! ''<Left>
nnoremap <Leader>sq :Ack!<CR>
nnoremap <Leader>sl :LAck!<CR>
vnoremap <Leader>sq y:Ack! '<C-r>"'<CR>
vnoremap <Leader>sl y:LAck! '<C-r>"'<CR>
nnoremap <leader>sc :nohlsearch<CR>

" pluginconfig
" ==========================================
" vim-jsx
" ==========================================
let g:jsx_ext_required = 0

" pluginconfig
" ==========================================
" git-fugitive
" ==========================================
if exists(":GdiffT") == 0
  command GdiffT tabedit %|Gdiff
endif
if exists(":GstatusT") == 0
  command GstatusT tabedit %|Gstatus
endif
nnoremap <leader>gs :GstatusT<CR>:resize 20<CR>
nnoremap <leader>gc :Gcommit<CR>
nnoremap <leader>gw :Gwrite<CR>
nnoremap <leader>gd :Gvdiff<CR>
nnoremap <leader>gp :Gpush<CR>
nnoremap <leader>gb :Gblame<CR>

" pluginconfig
" ==========================================
" NERDTree
" ==========================================
let g:NERDTreeIgnore=['node_modules', '\.git$', '\~$']

" pluginconfig
" ==========================================
" MatchTagAlways
" ==========================================
let g:mta_filetypes = { 'html' : 1, 'xhtml' : 1, 'xml' : 1, 'javascript.jsx' : 1 }

" pluginconfig
" ==========================================
" syntastic
" ==========================================
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 0
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 1
" javascript lint
let g:syntastic_mode_map = { 'mode': 'passive' }
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_javascript_eslint_exe = '$(npm bin)/eslint'
" eslint require following libs to work and .eslintrc files
" npm install --save-dev eslint eslint-config-airbnb babel-eslint eslint-plugin-react eslint-plugin-import eslint-plugin-jsx-a11y
" http://remarkablemark.org/blog/2016/09/28/vim-syntastic-eslint/

" pluginconfig
" ==========================================
" startify
" ==========================================
let g:startify_skiplist = [
            \ '.git/index',
            \ '.git/config',
            \ ]
let g:startify_bookmarks = [
            \ { 's': '~/Library/Application Support/EMR Connect/.settings.json' },
            \ ]
let g:startify_list_order = [
            \ ['   Sessions'],
            \ 'sessions',
            \ ['   Bookmarks'],
            \ 'bookmarks',
            \ ['   MRU'],
            \ 'files',
            \ ]
let g:startify_enable_special     = 1
let g:startify_change_to_dir      = 1
let g:startify_change_to_vcs_root = 1
let g:startify_relative_path      = 1
let g:startify_update_oldfiles    = 1
" let g:startify_custom_header = [
            " \'',
            " \'',
            " \'',
            " \'',
            " \'',
            " \'',
            " \'      (-(-_(-_-)_-)-)      ',
            " \ '',
            " \ "   it\'s ok to not feel ok",
            " \ '',
            " \ '',
            " \ '',
            " \ ]
let g:startify_session_delete_buffers = 1
let g:startify_session_persistence = 1
let g:startify_disable_at_vimenter= 0
let g:startify_session_before_save = ['silent! NERDTreeClose']
let g:startify_session_autoload = 1
nnoremap <leader>ss :Startify<CR>


" pluginconfig
" ==========================================
" neocomplete
" ==========================================
let g:acp_enableAtStartup = 0
let g:neocomplete#enable_at_startup = 1
let g:neocomplete#enable_smart_case = 1
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#sources#dictionary#dictionaries = {
    \ 'default' : '',
    \ 'vimshell' : $HOME.'/.vimshell_hist',
    \ 'scheme' : $HOME.'/.gosh_completions'
        \ }
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'
inoremap <expr><C-g>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return (pumvisible() ? "\<C-y>" : "" ) . "\<CR>"
  " For no inserting <CR> key.
  "return pumvisible() ? "\<C-y>" : "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
" Close popup by <Space>.
"inoremap <expr><Space> pumvisible() ? "\<C-y>" : "\<Space>"

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Enable heavy omni completion.
if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif
