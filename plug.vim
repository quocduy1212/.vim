call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-surround'
Plug 'bling/vim-airline'
Plug 'scrooloose/nerdcommenter'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-rails'
Plug 'godlygeek/tabular'
Plug 'mattn/emmet-vim'
Plug 'ConradIrwin/vim-bracketed-paste'
Plug 'pangloss/vim-javascript'
Plug 'vim-ruby/vim-ruby'
Plug 'tpope/vim-endwise'
Plug 'jiangmiao/auto-pairs'
Plug 'majutsushi/tagbar'
Plug 'mxw/vim-jsx'
Plug 'jlanzarotta/bufexplorer'
Plug 'vim-syntastic/syntastic'
Plug 'Shougo/neocomplete.vim'
Plug 'mhinz/vim-startify'
Plug 'ap/vim-css-color'
Plug 'tpope/vim-haml'
Plug 'szw/vim-maximizer'
Plug 'kchmck/vim-coffee-script'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'mileszs/ack.vim'
Plug 'yssl/QFEnter'
Plug 'easymotion/vim-easymotion'
Plug 'tpope/vim-repeat'
Plug 'mbbill/undotree'
Plug 'vimwiki/vimwiki'
Plug 'sbdchd/neoformat'
Plug 'prettier/vim-prettier'
Plug 'gregsexton/gitv', {'on': ['Gitv']}
Plug 'moll/vim-node'
Plug 'wellle/targets.vim'
Plug 'metakirby5/codi.vim'

" themes
Plug 'dracula/vim'
Plug 'morhetz/gruvbox'
Plug 'joshdick/onedark.vim'
Plug 'hukl/Smyck-Color-Scheme'
Plug 'liuchengxu/space-vim-dark'
Plug 'rakr/vim-one'
Plug 'ajmwagar/vim-deus'
Plug 'tyrannicaltoucan/vim-quantum'
Plug 'trevordmiller/nova-vim'
Plug 'ayu-theme/ayu-vim'
Plug 'junegunn/seoul256.vim'
Plug 'arcticicestudio/nord-vim'
Plug 'jnurmine/Zenburn'
Plug 'lifepillar/vim-solarized8'
Plug 'w0ng/vim-hybrid'
Plug 'raphamorim/lucario'
Plug 'jacoborus/tender.vim'
Plug 'kristijanhusak/vim-hybrid-material'
Plug 'haishanh/night-owl.vim'
Plug 'drewtempelmeyer/palenight.vim'

call plug#end()
