= tmux notes =

== tmux 2.6 on Centos 7 ==
* https://gist.github.com/zh-f/d1375e398fd4e624f421cc8e1741cf6d

== tmux copy mode ==
| key      | function                         |
|----------|----------------------------------|
| prefix [ | enter copy mode                  |
| Spacebar | start selection                  |
| Esc      | clear selection                  |
| Enter    | copy selection                   |
| prefix ] | paste contents of buffer_0       |
| q        | quit copy mode                   |
| J/K      | scroll page up/down line by line |
| C-d/u    | scroll have page down/up         |
| C-f/b    | next/previous page               |
| C-e/y    | Scroll page down/up              |
|          |                                  |

== tmux-resurrect ==
| shortcut   | action          |
|------------|-----------------|
| prefix C-s | save session    |
| prefix C-r | restore session |
| prefix s   | save session    |
| prefix r   | restore session |
|            |                 |

== tmux-sensible ==
| shortcut   | action                  |
|------------|-------------------------|
| prefix C-p | previous window         |
| prefix C-n | next window             |
| prefix R   | source .tmux.conf       |
| prefix b   | back to previous window |
|            |                         |

== tmux-pain-control ==

| key             | function                           |
|-----------------|------------------------------------|
| prefix C-h      | pane on the left                   |
| prefix C-l      | pane on the right                  |
| prefix C-j      | pane below                         |
| prefix C-k      | pane above                         |
| prefix H        | resize pane 5 cells to the left    |
| prefix L        | resize pane 5 cells to the right   |
| prefix J        | resize pane 5 cells down           |
| prefix K        | resize pane 5 cells to the left    |
| prefix vertical | split pane horizontally            |
| prefix -        | split pane vertically              |
| prefix \        | split pane full width horizontally |
| prefix _        | split pane full width vertically   |
| prefix <        | move window to left                |
| prefix >        | move window to right               |
|                 |                                    |
